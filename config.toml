baseurl = "https://www.eclipse.org/"
DefaultContentLanguage = "en"
title = "Eclipse Foundation"
theme = "eclipsefdn-hugo-solstice-theme"
metaDataFormat = "yaml"
disableKinds = ["taxonomy", "term"]
themesDir = "node_modules/"
enableRobotsTXT = true
pluralizeListTitles = false
disablePathToLower = true

[Params]
  google_tag_manager = "GTM-5WLCZXC"
  description = "The Eclipse Foundation provides our global community of individuals and organizations with a mature, scalable, and business-friendly environment for open source software collaboration and innovation."
  seo_title_suffix = " | The Eclipse Foundation"
  keywords = ["eclipse", "project", "plug-ins", "plugins", "java", "ide", "swt", "refactoring", "free java ide", "tools", "platform", "open source", "development environment", "development", "ide"]
  news_count = 3
  featured_content_publish_target = "eclipse_org"
  styles = "public/css/styles.css"
  gcse = "011805775785170369411:p3ec0igo0qq"
  gcse_result_url = "/home/search/"
  # eclipse_search_icon = "<i data-feather=\"search\" stroke-width=\"1\"></i>"
  js = "public/js/main.js?v1.0"
  header_wrapper_class = "header-wrapper"
  call_for_action_text = "Download"
  call_for_action_url = "/downloads/"
  share_img = "/images/logos/eclipse-foundation-400x400.png"
  favicon = "/favicon.ico"
  logo_width = "160"
  layout_style = "astro"
  hide_ad = false

#  logo_width = ""
#  header_left_classes = ""
#  main_menu_wrapper_classes = "col-sm-17 margin-top-25"
#  projects_working_group = ""
#  header_wrapper_class = "header-default-bg-img"
#  show_events = true
#  table_classes = "table table-bordered"
#  hide_cfa_same_page = true
#  show_collapsible_menu = true
  sidebar_layout = "default"

[taxonomies]
  tag = "tags"

[Author]
  name = "Eclipse Foundation"
  website = "https://www.eclipse.org"
  email = "webdev@eclipse-foundation.org"
  facebook = "eclipse.org"
  twitter = "eclipsefdn"
  youtube = "EclipseFdn"
  googleplus = "+Eclipse"
  linkedin = "company/eclipse-foundation/"

[permalinks]
  news = "/:sections/:year/:month/:day/:slug/"

[blackfriday]
  plainIDAnchors = true
  hrefTargetBlank = true

[[menu.main]]
  name = "Projects"
  weight = 1
  identifier = "projects"
  [menu.main.params]
    description = """
      The Eclipse Foundation is home to the Eclipse IDE, Jakarta EE, and
      hundreds of open source projects, including runtimes, tools,
      specifications, and frameworks for cloud and edge applications, IoT, AI,
      automotive, systems engineering, open processor designs, and many others.
    """

[[menu.main]]
  name = "Technologies"
  weight = 1
  identifier = "projects-technologies"
  parent = "projects"

[[menu.main]]
  name = "Developer Tools & IDEs"
  url = "/topics/ide/"
  weight = 1
  parent = "projects-technologies"

[[menu.main]]
  name = "Cloud Native"
  url = "/topics/cloud-native/"
  weight = 2
  parent = "projects-technologies"

[[menu.main]]
  name = "Edge & IoT"
  url = "/topics/edge-and-iot"
  weight = 3
  parent = "projects-technologies"

[[menu.main]]
  name = "Automotive & Mobility"
  url = "/topics/automotive-and-mobility/"
  weight = 4
  parent = "projects-technologies"

[[menu.main]]
  name = "Projects"
  weight = 2
  identifier = "projects-projects"
  parent = "projects"

[[menu.main]]
  name = "Project Finder"
  url = "https://projects.eclipse.org/"
  weight = 1
  parent = "projects-projects"

[[menu.main]]
  name = "Project Activity"
  url = "https://www.eclipse.org/projects/project_activity.php"
  weight = 2
  parent = "projects-projects"

[[menu.main]]
  name = "Project Resources"
  url = "/projects/resources"
  weight = 3
  parent = "projects-projects"

[[menu.main]]
  name = "Contribute"
  url = "/contribute/"
  weight = 4
  parent = "projects-projects"

[[menu.main]]
  name = "Supporters"
  weight = 2
  identifier = "supporters"
  [menu.main.params]
    description = """
      The Eclipse Foundation is an international non-profit association
      supported by our members, including industry leaders who value open
      source as a key enabler for their business strategies.
    """

[[menu.main]]
  name = "Membership"
  weight = 1
  identifier = "supporters-membership"
  parent = "supporters"

[[menu.main]]
  name = "Our Members"
  url = "/membership/exploreMembership.php"
  weight = 1
  parent = "supporters-membership"

[[menu.main]]
  name = "Member Benefits"
  url = "/membership/"
  weight = 2
  parent = "supporters-membership"

[[menu.main]]
  name = "Membership Levels & Fees"
  url = "/membership/#tab-levels"
  weight = 3
  parent = "supporters-membership"

[[menu.main]]
  name = "Membership Application"
  url = "https://membership.eclipse.org/application"
  weight = 4
  parent = "supporters-membership"

[[menu.main]]
  name = "Member Portal"
  url = "https://membership.eclipse.org/portal"
  weight = 5
  parent = "supporters-membership"

[[menu.main]]
  name = "Sponsorship"
  weight = 1
  identifier = "supporters-sponsorship"
  parent = "supporters"

[[menu.main]]
  name = "Sponsor"
  url = "/sponsor/"
  weight = 1
  parent = "supporters-sponsorship"

[[menu.main]]
  name = "Corporate Sponsorship"
  url = "/org/corporate_sponsors"
  weight = 2
  parent = "supporters-sponsorship"

[[menu.main]]
  name = "Sponsor a Collaboration"
  url = "/sponsor/collaboration/"
  weight = 3
  parent = "supporters-sponsorship"

[[menu.main]]
  name = "Collaborations"
  weight = 3
  identifier = "collaborations"
  [menu.main.params]
    description = """
      Whether you intend on contributing to Eclipse technologies that are
      important to your product strategy, or simply want to explore a specific
      innovation area with like-minded organizations, the Eclipse Foundation is
      the open source home for industry collaboration.
    """

[[menu.main]]
  name = "Industry Collaborations"
  weight = 1
  parent = "collaborations"
  identifier = "collaborations-industry-collaborations"

[[menu.main]]
  name = "About Industry Collaborations"
  url = "/collaborations/"
  weight = 1
  parent = "collaborations-industry-collaborations"

[[menu.main]]
  name = "Current Collaborations"
  url = "/org/workinggroups/explore.php"
  weight = 2
  parent = "collaborations-industry-collaborations"

[[menu.main]]
  name = "About Working Groups"
  url = "/org/workinggroups/about.php"
  weight = 3
  parent = "collaborations-industry-collaborations"

[[menu.main]]
  name = "About Interest Groups"
  url = "/collaborations/interest-groups/"
  weight = 4
  parent = "collaborations-industry-collaborations"

[[menu.main]]
  name = "Research Collaborations"
  identifier = "collaborations-research-collaborations"
  weight = 2
  parent = "collaborations"

[[menu.main]]
  name = "Research @ Eclipse"
  url = "/research/"
  weight = 1
  parent="collaborations-research-collaborations"

[[menu.main]]
  name = "Resources"
  weight = 4
  identifier = "resources"
  [menu.main.params]
    description = """
      The Eclipse community consists of individual developers and organizations
      spanning many industries. Stay up to date on our open source community
      and find resources to support your journey.
    """

[[menu.main]]
  name = "Open Source for Business"
  weight = 1
  parent = "resources"
  identifier = "resources-open-source"

[[menu.main]]
  name = "Business Value of Open Source"
  url = "/org/value/"
  weight = 1
  parent = "resources-open-source"

[[menu.main]]
  name = "Open Source Program Offices"
  url = "/os4biz/ospo/"
  weight = 2
  parent = "resources-open-source"

[[menu.main]]
  name = "What's Happening"
  weight = 2
  parent = "resources"
  identifier = "resources-happening"

[[menu.main]]
  name = "News"
  url = "https://newsroom.eclipse.org/"
  weight = 1
  parent = "resources-happening"

[[menu.main]]
  name = "Events"
  url = "https://events.eclipse.org/"
  weight = 2
  parent = "resources-happening"

[[menu.main]]
  name = "Newsletter"
  url = "/community/eclipse_newsletter/"
  weight = 3
  parent = "resources-happening"

[[menu.main]]
  name = "Press Releases"
  url = "https://newsroom.eclipse.org/news/press-releases"
  weight = 4
  parent = "resources-happening"

[[menu.main]]
  name = "Awards & Recognition"
  url = "/org/foundation/eclipseawards/"
  weight = 5
  parent = "resources-happening"

[[menu.main]]
  name = "Developer Resources"
  weight = 3
  parent = "resources"
  identifier = "resources-developer"

[[menu.main]]
  name = "Forum"
  url = "https://eclipse.org/forums/"
  weight = 1
  parent = "resources-developer"

[[menu.main]]
  name = "Wiki"
  url = "https://wiki.eclipse.org/"
  weight = 2
  parent = "resources-developer"

[[menu.main]]
  name = "Mailing Lists"
  url = "https://accounts.eclipse.org/mailing-list"
  weight = 3
  parent = "resources-developer"

[[menu.main]]
  name = "Blogs & Videos"
  url = "/blogs-and-videos/"
  weight = 4
  parent = "resources-developer"

[[menu.main]]
  name = "Marketplaces"
  url = "/resources/marketplaces/"
  weight = 5
  parent = "resources-developer"

[[menu.main]]
  name = "The Foundation"
  weight = 5
  identifier = "the-foundation"
  [menu.main.params]
    description = """
      The Eclipse Foundation provides our global community of individuals and
      organizations with a mature, scalable, and vendor-neutral environment for
      open source software collaboration and innovation.
    """

[[menu.main]]
  name = "About"
  weight = 1
  identifier = "the-foundation-about"
  parent = "the-foundation"

[[menu.main]]
  name = "About the Eclipse Foundation"
  url = "/org/"
  weight = 1
  parent="the-foundation-about"

[[menu.main]]
  name = "Board & Governance"
  url = "/org/governance"
  weight = 2
  parent="the-foundation-about"

[[menu.main]]
  name = "Staff"
  url = "/org/foundation/staff.php"
  weight = 3
  parent="the-foundation-about"

[[menu.main]]
  name = "Services"
  url = "/org/services"
  weight = 4
  parent="the-foundation-about"

[[menu.main]]
  name = "Legal"
  weight = 2
  identifier = "the-foundation-legal"
  parent = "the-foundation"

[[menu.main]]
  name = "Legal Policies"
  url = "/legal/"
  weight = 1
  parent="the-foundation-legal"

[[menu.main]]
  name = "Privacy Policy"
  url = "/legal/privacy.php"
  weight = 2
  parent="the-foundation-legal"

[[menu.main]]
  name = "Terms of Use"
  url = "/legal/termsofuse.php"
  weight = 3
  parent="the-foundation-legal"

[[menu.main]]
  name = "Copyright Agent"
  url = "/legal/copyright.php"
  weight = 4
  parent="the-foundation-legal"

[[menu.main]]
  name = "Eclipse Public License"
  url = "/legal/epl-2.0/"
  weight = 5
  parent="the-foundation-legal"

[[menu.main]]
  name = "More"
  weight = 3
  identifier = "the-foundation-more"
  parent = "the-foundation"

[[menu.main]]
  name = "Press Releases "
  url = "https://newsroom.eclipse.org/news/press-releases"
  weight = 1
  parent="the-foundation-more"

[[menu.main]]
  name = "Careers"
  url = "/careers/"
  weight = 2
  parent="the-foundation-more"

[[menu.main]]
  name = "Logos & Artwork"
  url = "/org/artwork/"
  weight = 3
  parent="the-foundation-more"

[[menu.main]]
  name = "Contact Us"
  url = "/org/foundation/contact.php"
  weight = 4
  parent="the-foundation-more"

[[menu.sidebar]]
  name = "Eclipse.org"
  url = "/home"
  weight = 2
  identifier = "home"

[[menu.sidebar]]
  parent ="home"
  name = "Downloads"
  url = "/downloads"
  #pre = "<i data-feather=\"users\"></i>" # https://feathericons.com/
  weight = 1

[[menu.sidebar]]
  parent ="home"
  name = "Resources"
  url = "https://wiki.eclipse.org/Eclipse_Articles,_Tutorials,_Demos,_Books,_and_More"
  #pre = "<i data-feather=\"users\"></i>" # https://feathericons.com/
  weight = 2

[[menu.sidebar]]
  parent ="home"
  name = "Projects"
  url = "/projects/"
  #pre = "<i data-feather=\"users\"></i>" # https://feathericons.com/
  weight = 2

[[menu.sidebar]]
  identifier = "documentation"
  name= "Getting Started"
  url = "/getting_started"
  weight = 1

[[menu.sidebar]]
  parent = "documentation"
  name = "New to Eclipse?"
  url = "/getting_started"
  pre = "<i class=\"fa fa-caret-right fa-fw\"></i>"
  weight = 2

[[menu.sidebar]]
  parent = "documentation"
  name = "Downloads"
  url = "/downloads"
  pre = "<i class=\"fa fa-caret-right fa-fw\"></i>"
  weight = 3

[[menu.sidebar]]
  parent = "documentation"
  name = "Eclipse Articles"
  url = "https://wiki.eclipse.org/Eclipse_Corner"
  pre = "<i class=\"fa fa-caret-right fa-fw\"></i>"
  weight = 4

[[menu.sidebar]]
  parent = "documentation"
  name = "Eclipse Forums"
  url = "/forums"
  pre = "<i class=\"fa fa-caret-right fa-fw\"></i>"
  weight = 5

[[menu.sidebar]]
  parent = "documentation"
  name = "Resources"
  url = "/resources"
  pre = "<i class=\"fa fa-caret-right fa-fw\"></i>"
  weight = 6

[[menu.sidebar]]
  identifier = "contribute"
  name= "Development Program"
  url = "/contribute/dev-program"
  weight = 1

[[menu.sidebar]]
  parent = "contribute"
  name = "Work Items"
  url = "https://projects.eclipse.org/development-efforts"
  weight = 1

[[menu.sidebar]]
  parent = "contribute"
  name = "FAQ"
  url = "/contribute/dev-program/faq"
  weight = 2

[[menu.sidebar]]
  identifier = "collaborations"
  name = "Industry Collaborations"
  url = "/collaborations/"
  weight = 1

[[menu.sidebar]]
  parent = "collaborations"
  name = "About Interest Groups"
  url = "/collaborations/interest-groups"
  weight = 1

[[menu.sidebar]]
  parent = "collaborations"
  name = "Interest Group Process"
  url = "/org/collaborations/interest-groups/process.php"
  weight = 2

[[menu.sidebar]]
  parent = "collaborations"
  name = "Explore our Interest Groups"
  url =  "/org/workinggroups/explore.php#interest-groups"
  weight = 3

[[menu.sidebar]]
  identifier = "security"
  name = "Security"
  url = "/security"
  weight = 1

[[menu.sidebar]]
  identifier = "security"
  name = "Security"
  url = "/security"
  weight = 1

[[menu.sidebar]]
  parent = "security"
  name = "Known Vulnerabilities"
  url = "/security/known"
  pre = "<i class=\"fa fa-caret-right fa-fw\"></i>"
  weight = 1

[[menu.sidebar]]
  parent = "security"
  name = "Mail the Security Team"
  url = "mailto:security@eclipse-foundation.org"
  pre = "<i class=\"fa fa-caret-right fa-fw\"></i>"
  weight = 1

[[menu.sidebar]]
  parent = "security"
  name = "Team Members"
  url = "/security/team"
  pre = "<i class=\"fa fa-caret-right fa-fw\"></i>"
  weight = 2

[[menu.sidebar]]
  parent = "security"
  name = "Policy"
  url = "/security/policy"
  pre = "<i class=\"fa fa-caret-right fa-fw\"></i>"
  weight = 3
