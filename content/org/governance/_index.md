---
title: Board & Governance
hide_sidebar: true
page_css_file: /public/css/org/governance.css
keywords: ["Eclipse Foundation board of directors", "board of directors elections", "architecture council", "Eclipse Foundation annual report", "policies and processes", "meeting minutes"]
---

{{< grid/div class="governance-tile-row margin-top-60 margin-bottom-60" isMarkdown="false" >}}

{{< pages/org/governance/tile class="bg-neutral-flat bg-hover-darker padding-30" title="Board of Directors" url="/org/foundation/directors.php">}}
The Eclipse Foundation Board of Directors oversees the policies and strategic
direction of the Foundation.
{{</ pages/org/governance/tile >}}

{{< pages/org/governance/tile class="bg-neutral-tinted-dark bg-hover-lighter padding-30" title="Board of Directors Election" url="/org/elections">}}
The Eclipse Foundation holds annual elections for board members representing
the Committer Members and the Contributing Members.
{{</ pages/org/governance/tile >}}

{{< pages/org/governance/tile class="bg-primary-light bg-hover-darker padding-30" title="Governance Documents" url="/org/documents/">}}
Explore the documents, processes, and additional policies that govern the
Eclipse Foundation.
{{</ pages/org/governance/tile >}}

{{< pages/org/governance/tile class="bg-primary-light bg-hover-darker padding-30" title="Annual Report" url="/org/foundation/reports/annual_report.php">}}
This report provides an overview of the past fiscal year at the Eclipse
Foundation, including a financial summary.
{{</ pages/org/governance/tile >}}

{{< pages/org/governance/tile class="bg-neutral-tinted-dark bg-hover-lighter padding-30" title="Architecture Council" url="/org/foundation/council.php">}}
Open source projects at the Eclipse Foundation are guided and coordinated by
the Architecture Council. Learn more about the council and its members.
{{</ pages/org/governance/tile >}}

{{< pages/org/governance/tile class="bg-neutral-flat bg-hover-darker padding-30" title="Meeting Minutes" url="/org/foundation/minutes.php">}}
Find the meeting minutes for the Board of Directors Meetings, General Assembly
Meetings, and Council Meetings.
{{</ pages/org/governance/tile >}}

{{</ grid/div >}}
