---
title: "Research Projects"
date: 2019-04-01T13:30:00-00:00
hide_sidebar: true
outputs:
    - HTML
    - JSON
cascade:
  container: bg-white container-fluid
---

{{< grid/section-container class=" padding-top-40 padding-bottom-40 margin-bottom-40" isMarkdown="false" >}}
    <h2 class="text-center margin-bottom-40">The Eclipse Foundation is a Partner in these Projects</h2>
    {{< eclipsefdn_projects is_static_source="true" url="/research/projects/index.json" templateId="tpl-projects-item-research" display_view_more="false" >}}
{{</ grid/section-container >}}

{{< mustache_js template-id="tpl-projects-item-research" path="/js/src/templates/research/tpl-projects-item-research.mustache" >}}
