---
title: "RobMoSys"
date: 2017-01-01T00:00:00-00:00
project_logo: "/research/images/research/r-projects/robmosys.png"
tags: ["Robotics", "Standard"]
homepage: "https://robmosys.eu/"
facebook: ""
linkedin: "https://www.linkedin.com/groups/8635121"
twitter: "https://twitter.com/RobMoSys/"
youtube: "https://www.youtube.com/channel/UCURqFtHgAPsXl9mB_QgbB0w"
funding_bodies: ["horizon2020"]
eclipse_projects: ["modeling.smartmdsd"]
project_topic: Robotics
summary: "An integrated approach built on top of the current code-centric robotic platforms."
hide_page_title: true
hide_sidebar: true
header_wrapper_class: "header-projects-bg-img"
description: "# **RobMoSys**

RobMoSys envisions an integrated approach built on top of the current code-centric robotic platforms, by applying model-driven methods and tools.
RobMoSys will enable the management of the interfaces between different robotics-related domains in an efficient and systematic way according to each system’s needs.


- RobMoSys aims to establish Quality-of-Service properties, enabling a composition-oriented approach while preserving modularity.

- RobMoSys will drive the non-competitive part of building a professional quality ecosystem by encouraging the community involvement.

- RobMoSys will elaborate many of the common robot functionalities based on broad involvement of the community via two Open Calls.



The [**RobMoSys Academy**](https://robmosys.eu/academy_/) provides a comprehensive set of online training material and tutorials to get familar with the RobMoSys methodology and tools!


RobMoSys was running from January 2017 to December 2020."
---

{{< grid/div class="container research-page-section">}}



## Eclipse Papyrus4Robotics
[Papyrus4Robotics](https://www.eclipse.org/papyrus/components/robotics/) is a graphical editing tool for robotic applications that complies with the RobMoSys approach. It manages complexity of robotics development by supporting composition-oriented engineering of robotics systems and separating the task into multiple tiers executed by different roles. It is based on [Eclipse Papyrus](https://www.eclipse.org/papyrus/), an industrial-grade open source Model-Based Engineering tool. Eclipse Papyrus has notably been used successfuly in industrial projects and is the base platform for several industrial modeling tools.

{{</ grid/div>}}

{{< grid/div class="bg-light-gray" isMarkdown="false">}}
{{< grid/div class="container research-page-section" >}}

## Eclipse SmartMDSD
[Eclipse SmartMDSD](https://projects.eclipse.org/projects/modeling.smartmdsd) is an Eclipse-based Integrated Development Environment (IDE) for robotics software development. The SmartMDSD Toolchain provides support and guidance to apply best-practices for the development of individual software building blocks, as well as their composition to robotics applications and systems. This project will maintain the eclipse-based tooling with its internal implementation (e.g. meta-models, code-generators).

# Consortium
* Le Commissariat à l’énergie atomique et aux énergies alternatives (CEA) - France
* Fachhochschule Ulm - Germany
* Katholieke Universiteit Leuven - Belgium
* Technische Universität München - Germany
* Siemens AG - Germany
* PAL Robotics SL - Spain
* Comau Spa - Italy
* Eclipse Foundation Europe GmbH - Germany
* EUnited AISBL - Belgium

{{</ grid/div>}}
{{</ grid/div>}}
