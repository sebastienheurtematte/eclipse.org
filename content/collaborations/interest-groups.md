---
title: "About Interest Groups"
date: 2022-09-21T10:30:17-04:00
description: ""
categories: []
keywords: []
slug: ""
aliases: []
toc: false
draft: false
#header_wrapper_class: ""
#seo_title: ""
#headline: ""
#subtitle: ""
#tagline: ""
#links: []
---
Interest groups are a light-weight, low-cost way for Eclipse Foundation members to collaborate on a common interest or domain in a vendor neutral manner based on the Eclipse Foundation’s governance structure, including its antitrust policy.

Find out more about interest groups, with these frequently asked questions:

1.  [What are Eclipse Foundation Interest Groups?](#1-what-are-eclipse-foundation-interest-groups)
2.  [Does Eclipse now offer two options for industry collaborations?](#2-does-this-mean-eclipse-offers-two-mechanisms-by-which-it-supports-industry-collaborations)
3.  [What are the major differences between these collaborations and which one is right for my initiative?](#3-what-are-the-major-differences-between-interest-groups-and-working-groups-how-do-i-choose-which-is-right-for-my-initiative)
4.  [How do I propose a new Interest Group?](#4-how-can-i-propose-to-create-an-eclipse-interest-group)
5.  [How many members are required to create an Eclipse Interest Group?](#5-how-many-members-are-required-in-order-to-create-an-interest-group)
6.  [Are there any fees associated with Interest Groups?](#6-are-there-any-fees-associated-with-interest-groups)
7.  [How to join an existing Interest Group?](#7-how-can-i-participate-in-an-existing-interest-group)
8.  [What is the governance structure of Interest Groups?](#8-what-is-the-governance-structure-of-an-interest-group)
9.  [How do Interest Groups make decisions?](#9-how-do-interest-groups-make-decisions)
10. [Is there a relationship between Interest Groups and Eclipse Projects?](#10-what-relationship-do-interest-groups-have-to-projects)
11. [Can Interest Groups create specifications and/or develop software?](#11-can-interest-groups-create-specifications-andor-develop-software)
12. [Are Interest Groups a pre-req to becoming a Working Group?](#12-are-interest-groups-a-prerequisite-to-becoming-a-working-group)
13. [Can Interest Groups graduate to Working Groups?](#13-can-interest-groups-graduate-to-working-groups)
14. [Are Interest Groups the same as Special Interest Groups?](#14-are-interest-groups-the-same-as-special-interest-groups-sigs)
15. [I’m interested, how do I find out more about the Eclipse Foundation industry collaborations?](#15-how-do-i-find-out-more-about-interest-groups-and-working-groups)


## 1. What are Eclipse Foundation Interest Groups?
Eclipse Foundation Interest Groups facilitate the collaboration between participating Eclipse Member organizations to drive shared innovation. Eclipse Interest Groups are a light-weight association of a subset of Members that can come together to share a common interest in a topic or domain in a vendor-neutral manner based on the Eclipse Foundation’s overall governance structure including its antitrust policy.

## 2. Does this mean Eclipse offers two mechanisms by which it supports industry collaborations?
Yes, the Eclipse Foundation now offers two mechanisms to foster open industry collaboration to develop and drive new industry platforms and initiatives. Like Eclipse Working Groups, Eclipse Interest Groups can also collaborate across organizational boundaries in a vendor-neutral structure to solve industry problems and to drive shared innovation.

## 3. What are the major differences between Interest Groups and Working Groups? How do I choose which is right for my initiative?  
Interest Groups, while modeled after Working Groups, are a very light-weight/self-governed  group. Participation in Interest Groups is self managed by Member organizations, and there are no formal participation agreements, budgets, or committees. In short, they provide a simpler mechanism for Member organizations to collaborate in a collegial, vendor-neutral manner on initiatives of shared interest. 

Working Groups are intended for industry collaborative initiatives that have the intent to invest in the collective initiative, typically via participation fees. The intent is for working groups to address such additional objectives as developing and promoting a joint brand, developing open source specifications, contributing resources to drive common roadmaps and/or platforms, etc.

From a pragmatic point of view, Working Groups leverage the [Eclipse Foundation Working Group Process](https://www.eclipse.org/org/workinggroups/process.php) to manage and drive the collaboration, and have Participation Agreements, Working Group Fees, Charters,Committees, Program Plans, Budgets, and so on. Interest Groups, on the other hand, due to their lighter-weight structure, have none of these. 

## 4. How can I propose to create an Eclipse Interest Group?
Existing members may propose to create an Interest Group by starting the process [here](https://projects.eclipse.org/node/add/interest-group). If your organization is not already a member, you must first [join the Eclipse Foundation](https://www.eclipse.org/membership/#tab-membership), and then follow the same process. Note that all participating member organizations must have also executed the Eclipse Member Committer and Contributor Agreement.

## 5. How many members are required in order to create an Interest Group?
There must be at least three Member organizations participating. Each Interest Group must designate one or more Interest Group Leads.

## 6. Are there any fees associated with Interest Groups?
No, Interest Groups do not require Members to pay any additional fees to participate.  However, you must be a Member of the Eclipse Foundation to participate. For more information, please visit our [Membership Page](https://www.eclipse.org/membership/). 

## 7. How can I participate in an existing Interest Group?
Existing members with an executed Eclipse Member Committer and Contributor Agreement may participate in any Interest Group of interest. Members may participate by declaring participation via the Interest Group’s mailing list.

## 8. What is the governance structure of an Interest Group?
The governance structure for Interest Groups is intentionally lightweight. Eclipse Interest Groups inherit and rely upon the Eclipse Foundation’s overall governance, sufficient to enable individuals from Member organizations to collaborate effectively while conforming to all Eclipse processes and policies, including the Eclipse Foundation Intellectual Property and Antitrust Policies. See the [Interest Group Process](https://www.eclipse.org/org/collaborations/interest-groups/process.php) for a full overview of applicable related governance documents.

All Interest Groups must produce agendas and minutes of all meetings and disseminate to the Interest Group’s mailing list.

## 9. How do Interest Groups make decisions?
Most actions taken by Interest Groups should be done so collegially by participants and based on lazy consensus.  However, the [Eclipse Foundation Interest Group Process](https://www.eclipse.org/org/collaborations/interest-groups/process.php) does include a means for decisions to be formalized, should the participants believe it is valuable to do so.  

## 10. What relationship do Interest Groups have to Projects?
Interest Groups may declare interest in any Eclipse project or projects, and may carry out activities that are in support of the success of those Eclipse project(s).

## 11. Can Interest Groups create specifications and/or develop software?
No, Interest Groups cannot create specifications nor develop software.  Interest Groups may, at their discretion, produce artifacts such as documents, whitepapers, architectures, blueprints, diagrams, presentation and the like; however, they must not develop software, software documentation, or specifications.

## 12. Are Interest Groups a Prerequisite to becoming a Working Group?
No, Interest Groups are not a prerequisite to becoming a Working Group. It is possible, however, for the members of an Interest Group to decide to create a new Working Group.  

## 13. Can Interest Groups graduate to Working Groups?
There is no direct correlation between Interest Groups and Working Groups, and thus no notion of graduation.  Rather, all Eclipse industry collaborations, be they Interest Groups or Working Groups, will persist for however long they serve the collective purpose of their Members. It is possible that, over time, the Members of a particular Interest Group may deem that it would be more useful to be constituted as a Working Group, and vice versa. Should this be the case, it is possible under the guidance of the Foundation’s staff to make such a change.  

## 14. Are Interest Groups the same as Special Interest Groups (SIGs)?
While similar, they are different.  Special Interest Groups are a collaborative governance structure that allows Members of a working group to collaborate on specific, focused aspects of the broader working group’s scope. As such, Members of a SIG must also be participants of the Working Group which has formed the SIG.

## 15. How do I find out more about Interest Groups and Working Groups?
Please contact us via [collaborations@eclipse-foundation.org](mailto:collaborations@eclipse-foundation.org).
