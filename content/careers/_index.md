---
title: Careers
date: 2004-02-02T18:54:43.927Z
layout: single
hide_sidebar: true
lastmod: 2022-05-24T13:16:59.860Z
description: Join the Eclipse Foundation team to make a difference in the open source community.
keywords:
  - careers
  - jobs
  - open source
  - open source jobs 
  - developer jobs
  - remote jobs
---

{{< grid/div class="row" isMarkdown="false">}}
  {{< grid/div class="col-md-13 margin-top-10" isMarkdown="false" >}}
    <h2>Current Openings</h2>
    {{< pages/careers/jazz-hr-basic-job-widget >}}
  {{</ grid/div >}}

{{< grid/div class="col-md-10 col-md-offset-1" >}}
## Why you should join the EF staff team
Join a collaborative team built around the core values of service, respect, professionalism, and collegiality. We work together to make a difference for our members and open source communities.

The Eclipse Foundation is global and offers *fully remote positions*, with staff located in Europe, Canada, and the United States.

We understand the importance of work-life balance and support this with many worker-based programs, including **Friday flex-time**, a **right-to-disconnect policy**, and **"Corporate Recharge" days**.

And of course, we offer highly competitive compensation along with a comprehensive benefits package.

## What we do at the Eclipse Foundation

Open source is one of the key drivers of innovation today. It touches all sectors of the economy and has a major impact on the software systems that each of us interact with every day. As a Belgian international nonprofit association, the Eclipse Foundation is one of the largest open source foundations in the world and acts as the steward for some of the most interesting and important projects in a wide range of technology areas. Each of our team members plays a direct and proactive role in the success of Eclipse and its projects and initiatives, and as captured by our most recent employee survey, the work done by our team is "dynamic, challenging, and impactful". Come be a part of our highly motivated and effective team!
{{</ grid/div >}}
{{</ grid/div >}}
